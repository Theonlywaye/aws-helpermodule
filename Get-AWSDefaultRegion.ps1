﻿function Get-AWSDefaultRegion
{
  $regions = Get-AWSRegion | Sort-Object Name
  Write-Host Select the region
  $regionsmenu = @{}
  for ($i = 1;$i -le $regions.count; $i++) 
  {
    Write-Host -Object "$i. $($regions.Name[$i-1])" -ForegroundColor 'green'
    $regionsmenu.Add($i,($regions.Name[$i-1]))
  }
  [int]$regionsans = Read-Host -Prompt 'Enter region'
  $regionselection = $regionsmenu.Item($regionsans)
  if (($regionsans -gt $regions.count) -or ($regionsans -eq $null) -or ($regionsans -eq 0))
  {
    Write-Host -ForegroundColor Red -Object "Couldn't determine which region you want. Please retry..."
    Start-Sleep -Seconds 1
    Get-AWSDefaultRegion
  }
  else
  {
    $global:regionselection = ($regions | Where-Object -FilterScript {
        $_.Name -eq $regionselection
    }).Region
  }
}