﻿Function Edit-AWSR53Records
{
  [CmdletBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter(Mandatory = $true,ValueFromPipeline = $true)][ValidateSet('Create','Edit','Delete')]
    $Action,
    
    [Parameter(Mandatory = $true,ValueFromPipeline = $true)]
    $ZoneID,

    [Parameter(Mandatory = $true,ValueFromPipeline = $true)][ValidateSet('A', 'SOA', 'PTR', 'MX', 'CNAME','TXT','SRV','SPF','AAAA','NS')]
    [String]$RecordType,

    [Parameter(Mandatory = $true)]
    [string]$Name,

    [Parameter(Mandatory = $true)]
    $value,

    [int]$TTL,
        
    # If wait is specified, the function will wait until AWS confirms that the change is in sync, 
    #meaning it has been written to the zone
    $wait = $false,

    # If wait is specified, waitinterval specifies the interval in miliseconds between each polling
    [int]$waitinterval = 1000,
    
    [Parameter(Mandatory = $true)]
    $AccessKey,
    
    [Parameter(Mandatory = $true)]
    $SecretKey,    
    
    [Parameter(Mandatory = $true)]
    $SessionToken    


  )
  
  if ($Action -eq 'Create') 
  {
    $create = New-Object -TypeName Amazon.Route53.Model.Change
    $create.Action = 'CREATE'
    $create.ResourceRecordSet = New-Object -TypeName Amazon.Route53.Model.ResourceRecordSet
    $create.ResourceRecordSet.Name = $Name
    $create.ResourceRecordSet.Type = $RecordType
    $create.ResourceRecordSet.TTL = $TTL
    $create.ResourceRecordSet.ResourceRecords.Add(@{
        Value = if( $RecordType -eq 'TXT' ) 
        {
          """$value"""
        }
        else 
        {
          $value
        }
    } )

    $params = @{
      HostedZoneId        = $ZoneID
      ChangeBatch_Comment = "This change batch creates a $($RecordType) record for $($Name) with the value of $($value)"
      ChangeBatch_Change  = $create
    }
    try
    {
      $result = Edit-R53ResourceRecordSet @params -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken -ErrorAction Stop
    }
    catch
    {
      if ( $_.Exception -match 'ARRDATAIllegalIPv4Address') 
      {
        Write-Warning -Message "Invalid IP address for record ($($Name)). Skipping this entry, correct error and try again"
      }
      elseif ( $_.Exception -match 'already exists') 
      {
        Write-Warning -Message "Record already exists for ($($Name)). Skipping creation for this entry, use edit flag if you want to update this entry."
      }
      elseif ( $_.Exception -match 'UnsupportedCharacter') 
      {
        Write-Warning -Message "Unsupported character for record ($($Name)). Skipping creation for this entry. Ensure there no spaces or illegal characters per DNS RFCs."
      }
      elseif ( $_.Exception -match 'is not permitted in zone') 
      {
        Write-Warning -Message "Unsupported entry ($($Name)). Skipping creation for this entry. Ensure the name you are using matches atleast <name>.<zone>"
      }      
      else 
      { 
        "Error was $_"
        $line = $_.InvocationInfo.ScriptLineNumber
        "Error was in Line $line"
      }
    }
  }
  elseif ($Action -eq 'Edit') 
  {
    $record = (Get-R53ResourceRecordSet -HostedZoneId $ZoneID -AccessKey $stsrole.Credentials.AccessKeyId -SecretKey $stsrole.Credentials.SecretAccessKey -SessionToken $stsrole.Credentials.SessionToken).ResourceRecordSets | Where-Object{
      $_.Name -like "*$($Name)*"
    }
    if ($record -ne $null) 
    {
      Write-Verbose -Message "Found $($record.Name) in $($zoneid). Deleting record and re-creating it"
      $delete = New-Object -TypeName Amazon.Route53.Model.Change
      $delete.Action = 'delete'
      $delete.ResourceRecordSet = New-Object -TypeName Amazon.Route53.Model.ResourceRecordSet
      $delete.ResourceRecordSet.Name = $record.Name
      $delete.ResourceRecordSet.Type = $record.Type
      $delete.ResourceRecordSet.TTL = $record.TTL
      $delete.ResourceRecordSet.ResourceRecords.Add(@{
          Value = if( $RecordType -eq 'TXT' ) 
          {
            """$($record.ResourceRecords.Value)"""
          }
          else 
          {
            $record.ResourceRecords.Value
          }
      } )
    
      $create = New-Object -TypeName Amazon.Route53.Model.Change
      $create.Action = 'CREATE'
      $create.ResourceRecordSet = New-Object -TypeName Amazon.Route53.Model.ResourceRecordSet
      $create.ResourceRecordSet.Name = $Name
      $create.ResourceRecordSet.Type = $RecordType
      $create.ResourceRecordSet.TTL = $TTL
      $create.ResourceRecordSet.ResourceRecords.Add(@{
          Value = if( $RecordType -eq 'TXT' ) 
          {
            """$value"""
          }
          else 
          {
            $value
          }
      } )

      $params = @{
        HostedZoneId        = $ZoneID
        ChangeBatch_Comment = "This change batch edits by deleting then creating a $($RecordType) record for $($Name) with the value of $($value)"
        ChangeBatch_Change  = $delete, $create
      }
      try
      {
        $result = Edit-R53ResourceRecordSet @params -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken -ErrorAction Stop
      }
      catch
      {
        if ( $_.Exception -match 'ARRDATAIllegalIPv4Address') 
        {
          Write-Warning -Message "Invalid IP address for record ($($Name)). Skipping this entry, correct error and try again"
        }
        elseif ( $_.Exception -match 'already exists') 
        {
          Write-Warning -Message "Record already exists for ($($Name)). Skipping creation for this entry, use edit flag if you want to update this entry."
        }
        elseif ( $_.Exception -match 'UnsupportedCharacter') 
        {
          Write-Warning -Message "Unsupported character for record ($($Name)). Skipping creation for this entry. Ensure there no spaces or illegal characters per DNS RFCs."
        }
        elseif ( $_.Exception -match 'is not permitted in zone') 
        {
          Write-Warning -Message "Unsupported entry ($($Name)). Skipping creation for this entry. Ensure the name you are using matches atleast <name>.<zone>"
        } 
        else 
        { 
          "Error was $_"
          $line = $_.InvocationInfo.ScriptLineNumber
          "Error was in Line $line"
        }
      }
    }
    else 
    {
      Write-Warning -Message "Record $($Name) doesn't exist, creating instead of updating"
      $create = New-Object -TypeName Amazon.Route53.Model.Change
      $create.Action = 'CREATE'
      $create.ResourceRecordSet = New-Object -TypeName Amazon.Route53.Model.ResourceRecordSet
      $create.ResourceRecordSet.Name = $Name
      $create.ResourceRecordSet.Type = $RecordType
      $create.ResourceRecordSet.TTL = $TTL
      $create.ResourceRecordSet.ResourceRecords.Add(@{
          Value = if( $RecordType -eq 'TXT' ) 
          {
            """$value"""
          }
          else 
          {
            $value
          }
      } )

      $params = @{
        HostedZoneId        = $ZoneID
        ChangeBatch_Comment = "This change batch edits by deleting then creating a $($RecordType) record for $($Name) with the value of $($value)"
        ChangeBatch_Change  = $delete, $create
      }
      try
      {
        $result = Edit-R53ResourceRecordSet @params -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken -ErrorAction Stop
      }
      catch
      {
        if ( $_.Exception -match 'ARRDATAIllegalIPv4Address') 
        {
          Write-Warning -Message "Invalid IP address for record ($($Name)). Skipping this entry, correct error and try again"
        }
        elseif ( $_.Exception -match 'already exists') 
        {
          Write-Warning -Message "Record already exists for ($($Name)). Skipping creation for this entry, use edit flag if you want to update this entry."
        }
        elseif ( $_.Exception -match 'UnsupportedCharacter') 
        {
          Write-Warning -Message "Unsupported character for record ($($Name)). Skipping creation for this entry. Ensure there no spaces or illegal characters per DNS RFCs."
        }
        elseif ( $_.Exception -match 'is not permitted in zone') 
        {
          Write-Warning -Message "Unsupported entry ($($Name)). Skipping creation for this entry. Ensure the name you are using matches atleast <name>.<zone>"
        } 
        else 
        { 
          "Error was $_"
          $line = $_.InvocationInfo.ScriptLineNumber
          "Error was in Line $line"
        }
      }
    }
  }
  elseif ($Action -eq 'delete') 
  {   
    $delete = New-Object -TypeName Amazon.Route53.Model.Change
    $delete.Action = 'delete'
    $delete.ResourceRecordSet = New-Object -TypeName Amazon.Route53.Model.ResourceRecordSet
    $delete.ResourceRecordSet.Name = $Name
    $delete.ResourceRecordSet.Type = $RecordType
    $delete.ResourceRecordSet.TTL = $TTL
    $delete.ResourceRecordSet.ResourceRecords.Add(@{
        Value = if( $RecordType -eq 'TXT' ) 
        {
          """$value"""
        }
        else 
        {
          $value
        }
    } )
    
    $params = @{
      HostedZoneId        = $ZoneID
      ChangeBatch_Comment = "This change batch deletes a $($RecordType) record for $($Name) with the value of $($value)"
      ChangeBatch_Change  = $delete
    }
    try
    {
      $result = Edit-R53ResourceRecordSet @params -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken -ErrorAction Stop
    }
    catch
    {
      if ( $_.Exception -match 'ARRDATAIllegalIPv4Address') 
      {
        Write-Warning -Message "Invalid IP address for record $($Name). Skipping this entry, correct error and try again"
      }
      else 
      { 
        "Error was $_"
        $line = $_.InvocationInfo.ScriptLineNumber
        "Error was in Line $line"
      }
    }
  }
  
  if ($result)
  {
    if ($wait)
    {
      Do
      {
        if ($SecondPoll)
        {
          Start-Sleep -Milliseconds $waitinterval
        }
        $status = Get-R53Change -Id $result.Id
        $SecondPoll = $true
        Write-Verbose "Waiting for changes to sync. Current status is $($status.Status.Value)"
      }
      Until ($status.Status.Value -eq 'INSYNC')
    }
    $status
  }
}
