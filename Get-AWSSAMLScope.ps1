﻿function Get-AWSSAMLScope
{
  Write-Host Select scope
  [int]$scopeselection = 0
  while ($scopeselection -lt 1 -or $scopeselection -gt 2) 
  {
    Write-Host -Object '1. Single AWS account' -ForegroundColor 'green'
    Write-Host -Object '2. All AWS accounts' -ForegroundColor 'green'
    [Int]$scopeselection = Read-Host -Prompt 'Please enter an option'
  }
  Switch( $scopeselection ) {
    1
    {
      Write-Verbose -Message 'Starting in single scope mode'
      Get-AWSSAMLAssertion -rolescope Single
    }
    2
    {
      Write-Verbose -Message 'Starting in all scope mode'      
      Get-AWSSAMLAssertion -rolescope All
    }
  }
}