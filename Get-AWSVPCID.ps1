﻿function Get-AWSVPCID
{
  [CmdletBinding(SupportsShouldProcess = $true)]
  Param (
   
    [Parameter(Mandatory = $true)]
    $AccessKey,
    
    [Parameter(Mandatory = $true)]
    $SecretKey,    
    
    [Parameter(Mandatory = $true)]
    $SessionToken
        
  )
  Write-Host "Select the VPC from account $(Get-IAMAccountAlias -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken)"
  $vpcs = Get-EC2Vpc -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken
  $vpcmenu = @{}
  for ($i = 1;$i -le $vpcs.count; $i++) 
  {
    if ($vpcs.count -gt 1) 
    {
      Write-Host -Object "$i. $($vpcs.VpcId[$i-1]) - CIDR: $($vpcs.CidrBlock[$i-1])" -ForegroundColor 'green'
      $vpcmenu.Add($i,($vpcs.VpcId[$i-1]))    
    }
    else 
    {
      Write-Host -Object "$i. $($vpcs.VpcId) - CIDR: $($vpcs.CidrBlock)" -ForegroundColor 'green'
      $vpcmenu.Add($i,($vpcs.VpcId))   
    }
  }
  [int]$vpcans = Read-Host -Prompt 'Enter VPC'
  $vpcselection = $vpcmenu.Item($vpcans)
  if (($vpcans -gt $vpcs.count) -or ($vpcans -eq $null) -or ($vpcans -eq 0))
  {
    Write-Host -ForegroundColor Red -Object "Couldn't determine which region you want. Please retry..."
    Start-Sleep -Seconds 1
    Get-AWSVPCID -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken
  }
  else
  {
    $global:vpc = $vpcs | Where-Object -FilterScript {
      $_.VpcId -eq $vpcselection
    }
  }
}