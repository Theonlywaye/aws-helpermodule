﻿# DO NOT MODIFY THIS FILE!
# THIS FILE WAS AUTOGENERATED BY ISESTEROIDS AND WILL BE OVERWRITTEN WHEN YOU EXPORT FUNCTIONS TO THIS MODULE.

# USE THIS FILE FOR ADDITIONAL MODULE CODE. THIS FILE WILL NOT BE OVERWRITTEN
# WHEN NEW CONTENT IS PUBLISHED TO THIS MODULE:
. $PSScriptRoot\init.ps1


# LOADING ALL FUNCTION DEFINITIONS:

. $PSScriptRoot\Edit-AWSR53Records.ps1
. $PSScriptRoot\Get-AWSDefaultRegion.ps1
. $PSScriptRoot\Get-AWSSAMLAssertion.ps1
. $PSScriptRoot\Get-AWSSAMLScope.ps1
. $PSScriptRoot\Get-AWSVPCID.ps1
. $PSScriptRoot\Get-AWSZoneID.ps1
