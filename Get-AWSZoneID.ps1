﻿function Get-AWSZoneID
{
  [CmdletBinding(SupportsShouldProcess = $true)]
  Param (
   
    [Parameter(Mandatory = $true)]
    $AccessKey,
    
    [Parameter(Mandatory = $true)]
    $SecretKey,    
    
    [Parameter(Mandatory = $true)]
    $SessionToken
        
  )
  Write-Host -Object "Select the zone from account $(Get-IAMAccountAlias -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken)"
  $zones = Get-R53HostedZones -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken
  if ($zones -ne $null) 
  {
    $zonemenu = @{}
    for ($i = 1;$i -le $zones.count; $i++) 
    {
      if ($zones.count -gt 1) 
      {
        Write-Host -Object "$i. $($zones.Name[$i-1]) - Private: $($zones.config.PrivateZone[$i-1])" -ForegroundColor 'green'
        $zonemenu.Add($i,($zones.Id[$i-1]))    
      }
      else 
      {
        Write-Host -Object "$i. $($zones.name) - Private: $($zones.config.PrivateZone)" -ForegroundColor 'green'
        $zonemenu.Add($i,($zones.Id))   
      }
    }
    [int]$zoneans = Read-Host -Prompt 'Enter zone'
    $zoneselection = $zonemenu.Item($zoneans)
    if (($zoneans -gt $zones.count) -or ($zoneans -eq $null) -or ($zoneans -eq 0))
    {
      Write-Host -ForegroundColor Red -Object "Couldn't determine which zone you want. Please retry..."
      Start-Sleep -Seconds 1
      Get-AWSZoneID -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken
    }
    else
    {
      $global:zone = $zones | Where-Object -FilterScript {
        $_.Id -eq $zoneselection
      }
    }
  }
  else 
  {
    Write-Verbose -Message "No zones in account $(Get-IAMAccountAlias -AccessKey $AccessKey -SecretKey $SecretKey -SessionToken $SessionToken)"
  }
}
