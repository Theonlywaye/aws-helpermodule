﻿function Get-AWSSAMLAssertion
{
  <#
      .DESCRIPTION
      Function to emulate a browser window in order to obtain a SAML assertion to pass to AWS to obtain temporary credentials
      .EXAMPLE
      To obtain temporary credentials for all available accounts
      Get-SAMLAssertion -url 'https://sso.server-federation.com/adfs/ls/IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices' -rolescope all
      To generate temporary credentials for one particular available account
      Get-SAMLAssertion -url 'https://sso.server-federation.com/adfs/ls/IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices' -rolescope single
  #>
  [CmdletBinding()]
  param
  (
    [Parameter(Mandatory = $false, Position = 0)]
    [System.String]
    $url = 'https://sso.server-federation.com/adfs/ls/IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices',
    [Parameter(Mandatory = $true)][ValidateSet('All','Single')]
    [System.String]
    $rolescope
  )
  
  $regex = '(?:<input name="SAMLResponse".+?value=")(.+?)(?:")'
  New-AWSCredentials -AccessKey 'Place holder access creds' -SecretKey 'Place holder secret key' | Set-AWSCredentials
  Set-DefaultAWSRegion -Region 'ap-southeast-2'
  
  $ie = New-Object -ComObject InternetExplorer.Application
  $ie.Visible = $true
  $ie.Height = '700' 
  $ie.Width = '800'
  $ie.Navigate2($url)
  while($ie.busy)
  {
    Start-Sleep -Seconds 1
  }
  $title = $ie.LocationName

  while ($title -eq 'Home Realm Discovery') 
  {
    Write-Verbose -Message "Waiting to get past $title"
    $title = $ie.LocationName
    Start-Sleep -Milliseconds 10
  }
    
  while ($title -eq 'Sign In') 
  {
    Write-Verbose -Message "Waiting to get past $title"
    $title = $ie.LocationName
    Start-Sleep -Milliseconds 10
  }
  
  while ($title -eq 'Duo Security') 
  {
    Write-Verbose -Message "Waiting to get past $title"
    $title = $ie.LocationName
    Start-Sleep -Milliseconds 10
  }
  
  while ($title -eq 'Working...') 
  {
    Write-Verbose -Message "Waiting to get past $title"
    $saml = $ie.document.body.innerhtml
    if ($saml -match $regex) 
    {
      Write-Verbose -Message 'Regex match for SAML token, breaking'
      break
    }
    $title = $ie.LocationName
    Start-Sleep -Milliseconds 10
  }
  
  Get-Process |
  Where-Object -FilterScript {
    $_.MainWindowHandle -eq $ie.HWND
  } |
  Stop-Process
  
  $samlassertion = $matches[1]
  $samldecode = [text.encoding]::utf8.getstring([convert]::FromBase64String($samlassertion))
  [xml]$SAMLXmlDocument = $samldecode
  
  $roles = @()
  $samlroles = $SAMLXmlDocument.Response.Assertion.AttributeStatement.Attribute | Where-Object -FilterScript {
    $_.Name -eq 'https://aws.amazon.com/SAML/Attributes/Role'
  }
  foreach ($role in $samlroles.AttributeValue) 
  {
    $split = $role.Split(',')
    $roles += New-Object -TypeName PSObject -Property @{
      roleArn      = $split[1]
      principalArn = $split[0]
    }   
  }
  
  
  if ($rolescope -eq 'Single') 
  {
    Write-Host Select your role
    $stsrolesmenu = @{}
    for ($i = 1;$i -le $roles.count; $i++) 
    {
      Write-Host -Object "$i. $($roles.RoleArn[$i-1])" -ForegroundColor 'green'
      $stsrolesmenu.Add($i,($roles.RoleArn[$i-1]))
    }
    [int]$stsrolesans = Read-Host -Prompt 'Enter role'
    $stsans = $stsrolesmenu.Item($stsrolesans)
    if (($stsrolesans -gt $roles.count) -or ($stsans -eq $null) -or ($stsrolesans -eq 0))
    {
      Write-Host -ForegroundColor Red -Object "Couldn't determine which role you want. Please retry..."
      Start-Sleep -Seconds 1
    }
    else
    {
      #Get temp credential for role
      $global:stsroles = @()
      $role = $roles | Where-Object -FilterScript {
        $_.RoleArn -eq $stsans
      }
      try
      {
        Write-Verbose -Message "Obtaining temporary credentials for role $($role.roleArn)"
        $global:stsroles += Use-STSRoleWithSAML -RoleArn $role.roleArn `
        -PrincipalArn $role.principalArn `
        -SAMLAssertion $samlassertion -ErrorAction Stop
      }
      catch
      {
        if ($_.Exception -match "Specified provider doesn't exist") 
        {
          Write-Warning -Message "Specified provider doesn't exist. Probable misconfiguration with the SAML provider for account $($role.roleArn)"
        }
        elseif ($_.Exception -match "User null is not authorized to perform: sts:AssumeRoleWithSAML on resource:") 
        {
          Write-Warning -Message "User null is not authorized to perform: sts:AssumeRoleWithSAM for account $($role.roleArn)"
        }
        elseif ($_.Exception -match "Not authorized to perform sts:AssumeRoleWithSAML") 
        {
          Write-Warning -Message "Not authorized to perform sts:AssumeRoleWithSAML for account $($role.roleArn)"
        }                  
        elseif ($_.Exception -match 'Token must be redeemed within 5 minutes of issuance') 
        {
          Write-Warning -Message 'SAML Assertion has expired. Start again.'
          Get-SAMLAssertion
        }
        else 
        {
          "Error was $_"
          $line = $_.InvocationInfo.globalLineNumber
          "Error was in Line $line"
        }
      } 
    }
  }
  else 
  {
    $global:stsroles = @()
    foreach ($role in $roles) 
    {
      try
      {
        Write-Verbose -Message "Obtaining temporary credentials for role $($role.roleArn)"
        $global:stsroles += Use-STSRoleWithSAML -RoleArn $role.roleArn `
        -PrincipalArn $role.principalArn `
        -SAMLAssertion $samlassertion -ErrorAction Stop
      }
      catch
      {
        if ($_.Exception -match "Specified provider doesn't exist") 
        {
          Write-Warning -Message "Specified provider doesn't exist. Probable misconfiguration with the SAML provider for account $($role.roleArn)"
        }
        elseif ($_.Exception -match "User null is not authorized to perform: sts:AssumeRoleWithSAML on resource:") 
        {
          Write-Warning -Message "User null is not authorized to perform: sts:AssumeRoleWithSAM for account $($role.roleArn)"
        }
        elseif ($_.Exception -match "Not authorized to perform sts:AssumeRoleWithSAML") 
        {
          Write-Warning -Message "Not authorized to perform sts:AssumeRoleWithSAML for account $($role.roleArn)"
        }                  
        elseif ($_.Exception -match 'Token must be redeemed within 5 minutes of issuance') 
        {
          Write-Warning -Message 'SAML Assertion has expired. Start again.'
          Get-SAMLAssertion
        }
        else 
        {
          "Error was $_"
          $line = $_.InvocationInfo.globalLineNumber
          "Error was in Line $line"
        }
      }
    }
  }
}